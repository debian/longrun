/*
 * longrun - Transmeta(TM) Crusoe(TM) LongRun(TM) utility
 *
 * Copyright (C) 2001  Transmeta Corporation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#define VERSION "0.9"
#define REVDATE "2001-02-14"

#include <ctype.h>
#include <errno.h>
#include <fcntl.h>
#include <getopt.h>
#include <stdarg.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/io.h>
#include <sys/sysmacros.h>
#include <locale.h>
#define __USE_UNIX98	/* for pread/pwrite */
#define __USE_FILE_OFFSET64 /* we should use 64 bit offset for pread/pwrite */
#include <unistd.h>

#include <libintl.h>
#define _(Text) gettext(Text)

#define MSR_DEVICE "/dev/cpu/0/msr"
#define MSR_TMx86_LONGRUN	0x80868010
#define MSR_TMx86_LONGRUN_FLAGS	0x80868011

#define MSR_TMx86_LRTI_READOUT	0x80868018
#define MSR_TMx86_LRTI_PERF	0x80868019
#define MSR_TMx86_LRTI_VOLT_MHZ	0x8086801A
#define MSR_TMx86_LRTI_DIV_MEM	0x8086801B
#define MSR_TMx86_LRTI_DIV_IO	0x8086801C
#define MSR_TMx86_LRTI_GATE	0x8086801D

#define MSR_TMx86_LRTI_SDR_MISC	0x80868030
#define MSR_TMx86_LRTI_SDR_TIF	0x80868031
#define MSR_TMx86_LRTI_SDR_MRS	0x80868032
#define MSR_TMx86_LRTI_SDR_LBT	0x80868033
#define MSR_TMx86_LRTI_DDR_MISC_TIF	0x80868034
#define MSR_TMx86_LRTI_DDR_MRS0	0x80868035
#define MSR_TMx86_LRTI_DDR_MRS1_MRS2	0x80868036
#define MSR_TMx86_PFTI_MING	0x80868037

#define LONGRUN_MASK(x)		((x) & 0x0000007f)
#define LONGRUN_RESERVED(x)	((x) & 0xffffff80)
#define LONGRUN_WRITE(x, y)	(LONGRUN_RESERVED(x) | LONGRUN_MASK(y))

#define CPUID_DEVICE "/dev/cpu/0/cpuid"
#define CPUID_TMx86_VENDOR_ID		0x80860000
#define CPUID_TMx86_PROCESSOR_INFO	0x80860001
#define CPUID_TMx86_LONGRUN_STATUS	0x80860007
#define CPUID_TMx86_FEATURE_LONGRUN(x)	((x) & 0x02)
#define CPUID_TMx86_FEATURE_LRTI(x)    ((x) & 0x08)

/* Advanced Thermal Management */
#define LR_NORTHBRIDGE "/proc/bus/pci/00/00.0"
#define ATM_ADDRESS	0xa8
#define ATM_EN(x)	(((x) & 0x10) >> 4)
#define ATM_LVL(x)	(((x) & 0x0e) >> 1)
#define LR_EN(x)	((x) & 0x01)

char *progname;			/* name of program */

int msr_fd;			/* MSR file descriptor */
char *msr_device;		/* MSR device name */

int cpuid_fd;			/* CPUID file descriptor */
char *cpuid_device;		/* CPUID device name */

int opt_verbose = 0;		/* verbosity */

void usage(int status) {
	FILE *stream = status ? stderr : stdout;

	fprintf(stream,
		_("%s %s (%s)\n"
		"usage: %s [-c device] [-m device] [-hlpv] [-f flag] [-s low high] [-t tlx]\n"
		" -c device     set CPUID device\n"
		" -m device     set MSR device\n"
		" -h            print this help\n"
		" -l            list LongRun information about available performance levels\n"
		" -p            print current LongRun settings and status\n"
		" -v            be more verbose\n"
		" -f [flag]     set a LongRun mode flag\n"
		" -s low high   set current LongRun performance window (0 to 100)\n"
		" -t tlx        set current LongRun Thermal Extensions setting (0 to 8, 8 = off)\n"
		"\n"
		"supported flags:\n"
		" economy       set economy mode (turn off performance mode)\n"
		" performance   set performance mode (turn off economy mode)\n"),
		progname, VERSION, REVDATE, progname);

	exit(status);
}

char *my_basename (const char *filename)
{
	char *base = strrchr (filename, '/');

	return base ? base + 1 : (char *) filename;
}

void error_warn (const char *fmt, ...)
{
	va_list arg_ptr;

	va_start(arg_ptr, fmt);
	fprintf(stderr, "%s: ", progname);
	vfprintf(stderr, fmt, arg_ptr);
	fprintf(stderr, ": %s\n", strerror(errno));
	va_end(arg_ptr);
}

void error_die (const char *fmt, ...)
{
	va_list arg_ptr;

	va_start(arg_ptr, fmt);
	fprintf(stderr, "%s: ", progname);
	vfprintf(stderr, fmt, arg_ptr);
	fprintf(stderr, ": %s\n", strerror(errno));
	va_end(arg_ptr);
	exit(1);
}

/* read Advanced Thermal Management definitions register */
void read_atm(uint8_t *atm)
{
	int nb;

	nb = open(LR_NORTHBRIDGE, O_RDONLY);
	if (nb < 0) {
		error_warn(_("error opening %s"), LR_NORTHBRIDGE);
		if (errno == ENOENT) {
			fprintf(stderr, _("make sure /proc is mounted\n"));
		}
		exit(1);
	}
	if (pread(nb, atm, 1, ATM_ADDRESS) != 1) {
		error_die(_("error reading %s"), LR_NORTHBRIDGE);
	}
	close(nb);
}

/* write Advanced Thermal Management definitions register */
void write_atm(uint8_t *atm)
{
	int nb;

	nb = open(LR_NORTHBRIDGE, O_RDWR);
	if (nb < 0) {
		error_warn(_("error opening %s"), LR_NORTHBRIDGE);
		if (errno == ENOENT) {
			fprintf(stderr, _("make sure /proc is mounted\n"));
		}
		exit(1);
	}
	if (pwrite(nb, atm, 1, ATM_ADDRESS) != 1) {
		error_die(_("error reading %s"), LR_NORTHBRIDGE);
	}
	close(nb);
}

/* note: if an output is NULL, then don't set it */
void read_msr(loff_t address, int *lower, int *upper)
{
	uint32_t data[2];

	if (pread(msr_fd, &data, 8, address) != 8) {
		error_die(_("error reading %s"), msr_device);
	}

	if (lower) *lower = data[0];
	if (upper) *upper = data[1];
}

void write_msr(loff_t address, int lower, int upper)
{
	uint32_t data[2];

	data[0] = (uint32_t) lower;
	data[1] = (uint32_t) upper;

	if (pwrite(msr_fd, &data, 8, address) != 8) {
		error_die(_("error writing %s"), msr_device);
	}
}

/* note: if an output is NULL, then don't set it */
void read_cpuid(loff_t address, int *eax, int *ebx, int *ecx, int *edx)
{
	uint32_t data[4];

	if (pread(cpuid_fd, &data, 16, address) != 16) {
		error_die(_("error reading %s"), cpuid_device);
	}

	if (eax) *eax = data[0];
	if (ebx) *ebx = data[1];
	if (ecx) *ecx = data[2];
	if (edx) *edx = data[3];
}

void check_cpu()
{
	int eax, ebx, ecx, edx;

	/* test for "TransmetaCPU" */
	read_cpuid(CPUID_TMx86_VENDOR_ID, &eax, &ebx, &ecx, &edx);
	if (ebx != 0x6e617254 || ecx != 0x55504361 || edx != 0x74656d73) {
		fprintf(stderr, _("%s: not a Transmeta x86 CPU\n"), progname);
		exit(1);
	}

	/* test for LongRun feature flag */
	read_cpuid(CPUID_TMx86_PROCESSOR_INFO, &eax, &ebx, &ecx, &edx);
	if (!CPUID_TMx86_FEATURE_LONGRUN(edx)) {
		printf(_("LongRun: unsupported\n"));
		exit(0);
	}
}

void print_lrti_info()
{
	int max;
	int junk;
       int max_voltage, max_mhz;
       double max_power;

	read_msr(MSR_TMx86_LRTI_READOUT, &junk, &max);

	write_msr(MSR_TMx86_LRTI_READOUT, 0, max);
	read_msr(MSR_TMx86_LRTI_VOLT_MHZ, &max_mhz, &max_voltage);

       max_power = ((double)max_mhz * max_voltage) * max_voltage;

	printf(_("# %%   MHz  Volts  usage  SDR  DDR  PCI\n"));

	int i;
	for (i = max; i >= 0; i--)
	{
               int percent, mhz, voltage;
               double power;
		int ddr_mem_div, sdr_mem_div, ddr_freq, sdr_freq;
		int io_div;
		write_msr(MSR_TMx86_LRTI_READOUT, i, max);

		read_msr(MSR_TMx86_LRTI_PERF, &percent, &junk);
		printf(_("%3d "), percent);

		read_msr(MSR_TMx86_LRTI_VOLT_MHZ, &mhz, &voltage);
		printf(_("%5d %6.3f "), mhz, (voltage / 1000.0));

                power = ((double)mhz * voltage) * voltage;

               printf(_("%6.3f "), ((double) power) / max_power);

		// TODO:  Watch for zeros
		read_msr(MSR_TMx86_LRTI_DIV_MEM, &sdr_mem_div, &ddr_mem_div);

		sdr_freq = 0;
		ddr_freq = 0;

		if (sdr_mem_div > 0)
		{
			sdr_freq = mhz / sdr_mem_div;
		}

		if (ddr_mem_div > 0)
		{
			ddr_freq = mhz / ddr_mem_div;
		}

		printf(_("%4d %4d "), sdr_freq, ddr_freq);

		read_msr(MSR_TMx86_LRTI_DIV_IO, &io_div, &junk);
               if (io_div) {
                       printf(_("%4d\n"), mhz / io_div);
               }
               else {
                       printf("%4s\n", "--");
               }

                /*
                // Most of the rest of these flags don't appear too interesting
		int eax, ebx;
		read_msr(MSR_TMx86_LRTI_GATE, &eax, &ebx);
		printf(_("MSR_TMx86_LRTI_GATE: %d %d\n"), eax, ebx);

		read_msr(MSR_TMx86_LRTI_SDR_MISC, &eax, &ebx);
		printf(_("MSR_TMx86_LRTI_SDR_MISC: %x %x\n"), eax, ebx);

		read_msr(MSR_TMx86_LRTI_SDR_TIF, &eax, &ebx);
		printf(_("MSR_TMx86_LRTI_SDR_TIF: %d %d\n"), eax, ebx);

		read_msr(MSR_TMx86_LRTI_SDR_MRS, &eax, &ebx);
		printf(_("MSR_TMx86_LRTI_SDR_MRS: %d %d\n"), eax, ebx);

		read_msr(MSR_TMx86_LRTI_SDR_LBT, &eax, &ebx);
		printf(_("MSR_TMx86_LRTI_SDR_LBT: %d %d\n"), eax, ebx);

		read_msr(MSR_TMx86_LRTI_DDR_MISC_TIF, &eax, &ebx);
		printf(_("MSR_TMx86_LRTI_DDR_MISC_TIF: %x %x\n"), eax, ebx);

		read_msr(MSR_TMx86_LRTI_DDR_MRS0, &eax, &ebx);
		printf(_("MSR_TMx86_LRTI_DDR_MRS0: %d %d\n"), eax, ebx);

		read_msr(MSR_TMx86_LRTI_DDR_MRS1_MRS2, &eax, &ebx);
		printf(_("MSR_TMx86_LRTI_DDR_MRS1_MRS2: %d %d\n"), eax, ebx);

		read_msr(MSR_TMx86_PFTI_MING, &eax, &ebx);
		printf(_("MSR_TMx86_PFTI_MING: %d %d\n"), eax, ebx);
                */
	}
}

void old_list_longrun()
{
	int i, save_lower, save_upper, pct_in, pct_last, steps, freq_max;
	int eax, ebx, ecx;
	float power_max, power_ratio;
	int *perf, *mhz, *volts;

	/* save current settings */
	read_msr(MSR_TMx86_LONGRUN, &save_lower, &save_upper);

	/* find freq_max */
	read_cpuid(CPUID_TMx86_PROCESSOR_INFO, 0, 0, &freq_max, 0);

	/* setup for probing */
	write_msr(MSR_TMx86_LONGRUN, LONGRUN_WRITE(save_lower, 0),
		  LONGRUN_WRITE(save_upper, 0));
	read_cpuid(CPUID_TMx86_LONGRUN_STATUS, &eax, 0, 0, 0);
	steps = (((freq_max - eax)) / (100.0 / 3.0) + 0.5); /* 33 MHz steps */
	perf = malloc(sizeof(int) * (steps + 1));
	mhz = malloc(sizeof(int) * (steps + 1));
	volts = malloc(sizeof(int) * (steps + 1));
	for (i = 0; i <= steps; i++) {
		mhz[i] = 0;
	}

	/* probe */
	pct_last = -1;
	for (i = 0; i <= steps; i++) {
		pct_in = ((float) i * (100.0 / (float) steps));
		write_msr(MSR_TMx86_LONGRUN, LONGRUN_WRITE(save_lower, pct_in),
			  LONGRUN_WRITE(save_upper, pct_in));
		read_cpuid(CPUID_TMx86_LONGRUN_STATUS, &eax, &ebx, &ecx, 0);
		if (opt_verbose)
			printf(_("# set %d, got %d\n"), pct_in, ecx);
		if (pct_last < ecx) {
			perf[i] = ecx;
			mhz[i] = eax;
			volts[i] = ebx;
		}
		if (ecx == 100)
			break;
		if (ecx > pct_in)
			i++;
		pct_last = ecx;
	}

	/* find power_max */
	power_max = mhz[i] * volts[i] * volts[i];

	printf(_("# %%   MHz  Volts  usage\n"));
	/* print results */
	for (i = 0; i <= steps; i++) {
		if (mhz[i]) {
			power_ratio = mhz[i] * volts[i] * volts[i] / power_max;
			printf(_("%3d %5d %6.3f %6.3f\n"),
			       perf[i], mhz[i], volts[i] / 1000.0, power_ratio);
		}
	}

	free(perf);
	free(mhz);
	free(volts);

	/* restore current settings */
	write_msr(MSR_TMx86_LONGRUN, save_lower, save_upper);
}

void list_longrun()
{
	int eax, ebx, ecx, edx;

	read_cpuid(CPUID_TMx86_PROCESSOR_INFO, &eax, &ebx, &ecx, &edx);
	if (CPUID_TMx86_FEATURE_LRTI(edx)) 
	{
		print_lrti_info();
	}
	else
	{
		old_list_longrun();
	}
}

void get_longrun(int *low, int *high)
{
	read_msr(MSR_TMx86_LONGRUN, low, high);
	/* only look at desired bits */
	*low = LONGRUN_MASK(*low);
	*high = LONGRUN_MASK(*high);
}

void set_longrun(int low, int high)
{
	int lower, upper;

	read_msr(MSR_TMx86_LONGRUN, &lower, &upper);
	write_msr(MSR_TMx86_LONGRUN, LONGRUN_WRITE(lower, low),
		  LONGRUN_WRITE(upper, high));
	if (opt_verbose) {
		printf(_("Setting performance window: %d to %d\n"), low, high);
	}
}

void set_longrun_atm(int level)
{
	uint8_t flags = 0x00;
	if (level != 8)
	{
		// LTX codes constitute bits 0-5, with the middle three bits
		// indicating the TLX setting
		flags = 0x11;
		flags |= (level & 0x7)<<1;
	}

	write_atm(&flags);
	if (opt_verbose) {
		printf(_("Setting thermal extensions: %x\n"), flags);
	}
}

void set_longrun_flag(char *flag)
{
	int lower, upper;

	read_msr(MSR_TMx86_LONGRUN_FLAGS, &lower, &upper);
	if (!strcmp(flag, "economy")) {
		write_msr(MSR_TMx86_LONGRUN_FLAGS, lower & 0xfffffffe, upper);
		if (opt_verbose) {
			printf(_("Setting flag: economy\n"));
		}
	}
	else if (!strcmp(flag, "performance")) {
		write_msr(MSR_TMx86_LONGRUN_FLAGS, (lower & 0xffffffff) | 0x1,
			  upper);
		if (opt_verbose) {
			printf(_("Setting flag: performance\n"));
		}
	}
	else {
		usage(1);
	}
}

void print_longrun()
{
	uint8_t atm;		/* Advanced Thermal Management */
	int lower, upper, percent;

	/* read the current performance level first */
	read_cpuid(CPUID_TMx86_LONGRUN_STATUS, 0, 0, &percent, 0);

	read_atm(&atm);

	printf(_("LongRun: %s\n"), LR_EN(atm) ? _("enabled") : _("disabled"));
	printf(_("LongRun Thermal Extensions (LTX): %s\n"), ATM_EN(atm) ? _("active") : _("inactive"));
	if (ATM_EN(atm)) {
		printf(_("LTX setting: "));
		switch(ATM_LVL(atm)) {
		case 0:
		case 1:
			printf(_("reserved\n")); break;
		case 2:
			printf(_("75%% reduction\n")); break;
		case 3:
			printf(_("62.5%% reduction\n")); break;
		case 4:
			printf(_("50%% reduction\n")); break;
		case 5:
			printf(_("37.5%% reduction\n")); break;
		case 6:
			printf(_("25%% reduction\n")); break;
		case 7:
			printf(_("12.5%% reduction\n")); break;
		}
	}
	get_longrun(&lower, &upper);
	printf(_("Current performance window: %d to %d\n"), lower, upper);
	printf(_("Current performance level: %d\n"), percent);
	read_msr(MSR_TMx86_LONGRUN_FLAGS, &lower, &upper);
	printf(_("LongRun flags: %s\n"), (lower & 1) ? _("performance") : _("economy"));
}

int main(int argc, char *argv[])
{
	int low, high;
	int atm;
	int g;
	char *opt_flag = NULL;
	int opt_list = 0;
	int opt_print = 0;
	int opt_set = 0;
	int opt_atm = 0;

	if (argc)
		progname = my_basename(argv[0]);
	else
		progname = "longrun";

	setlocale (LC_ALL, "");
	bindtextdomain (progname, LOCALEDIR);
	textdomain (progname);

	msr_device = MSR_DEVICE;
	cpuid_device = CPUID_DEVICE;

	/* command line options */
	while ((g = getopt(argc, argv, "c:f:m:hlpstv")) != EOF) {
		switch (g) {
		case 'c':
			cpuid_device = optarg;
			break;
		case 'f':
			opt_flag = optarg;
			break;
		case 'm':
			msr_device = optarg;
			break;
		case 'h':
			usage(0);
			break;
		case 'l':
			opt_list = 1;
			break;
		case 'p':
			opt_print = 1;
			break;
		case 's':
			opt_set = 1;
			break;
		case 't':
			opt_atm = 1;
			break;
		case 'v':
			opt_verbose++;
			break;
		}
	}

	/* Modified for Debian to allow concurrent use of the -s and -f
	 * switches -- JEH */
	if (opt_list + opt_print + opt_atm + (opt_set || (opt_flag ? 1 : 0)) != 1)
		usage(1);

	if (opt_set && (optind + 2 != argc))
		usage(1);

	if (opt_atm && (optind + 1 != argc))
		usage(1);

	if (geteuid()) {
		fprintf(stderr, _("%s: must be run as root\n"), progname);
		exit(1);
	}

	if ((cpuid_fd = open(cpuid_device, O_RDWR)) < 0) {
		error_warn(_("error opening %s"), cpuid_device);
		if (errno == ENODEV) {
			fprintf(stderr, _("make sure your kernel was compiled with CONFIG_X86_CPUID=y\n"));
		}
		exit(1);
	}

	if ((msr_fd = open(msr_device, O_RDWR)) < 0) {
		error_warn(_("error opening %s"), msr_device);
		if (errno == ENODEV) {
			fprintf(stderr, _("make sure your kernel was compiled with CONFIG_X86_MSR=y\n"));
		}
		exit(1);
	}

	check_cpu();

	if (opt_list) {
		list_longrun();
		exit(0);
	}
	if (opt_set) {
		low = strtol(argv[optind], NULL, 10);
		high = strtol(argv[optind + 1], NULL, 10);
		set_longrun(low, high);
	}
	if (opt_atm) {
		atm = strtol(argv[optind], NULL, 10);
		set_longrun_atm(atm);
	}
	if (opt_flag) {
		set_longrun_flag(opt_flag);
	}
	if (opt_print || opt_verbose) {
		print_longrun();
	}

	exit(0);
}
