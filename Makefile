include Makeconfig

all: longrun README stamp-po

longrun: longrun.c
	gcc -DLOCALEDIR=\"$(LOCALEDIR)\" -g -O2 -W -Wall -o longrun longrun.c

README: longrun.1
	groff -Tascii -man longrun.1 | col -bx > README

stamp-po:
	make -C po
	touch stamp-po

install: all
	mkdir -p $(DESTDIR)$(BINDIR)
	install -o root -g root -m 755 longrun $(DESTDIR)$(BINDIR)/longrun
	mkdir -p $(DESTDIR)$(MANDIR)/man1
	install -m 644 longrun.1 $(DESTDIR)$(MANDIR)/man1/longrun.1
	mkdir -p $(DESTDIR)$(MANDIR)/ja/man1
	install -m 644 longrun.1.ja $(DESTDIR)$(MANDIR)/ja/man1/longrun.1
	make -C po install

clean:
	-rm -f longrun README stamp-po *~
	make -C po clean
